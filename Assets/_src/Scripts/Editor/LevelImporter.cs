﻿using System.IO;
using StepByStep.TileSystem.DTO;
using UnityEditor.AssetImporters;
using UnityEngine;


namespace StepByStep.Editor
{
	[ScriptedImporter(1, FileExtensions.Level)]
	public class LevelImporter : ScriptedImporter
	{
		public override void OnImportAsset(AssetImportContext ctx)
		{
			var asset = new TextAsset(File.ReadAllText(ctx.assetPath));
			ctx.AddObjectToAsset("Text", asset);
			ctx.SetMainObject(asset);
		}
	}
}
