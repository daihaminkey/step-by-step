namespace StepByStep.TileSystem.DTO
{
	public class LevelMapDto
	{
		public TileDto[,] Map;


		public LevelMapDto(TileDto[,] map)
		{
			Map = map;
		}
	}
}
