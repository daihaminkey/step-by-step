using System.Collections.Generic;
using System.IO;
using System.Text;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using StepByStep.TileSystem.DTO;
using UnityEditor;
using UnityEngine;


namespace StepByStep.Editor
{
	public class LevelExplorerWindow : OdinEditorWindow
	{
		private static readonly JsonSerializerSettings _Settings = new()
		{
			TypeNameHandling = TypeNameHandling.All,
			MissingMemberHandling = MissingMemberHandling.Ignore,
		};


		[SerializeField]
		[BoxGroup("Asset")]
		[AssetSelector(Filter = "l:" + FileExtensions.Level)]
		[OnValueChanged(nameof(OnAssetSelect))]
		[InlineButton(nameof(ClearPath), ShowIf = "_asset", Label = "Clear")]
		[InlineButton(nameof(SaveClickHandler), ShowIf = "_asset", Label = "Save")]
		[InlineButton(nameof(LoadAsset), ShowIf = "_asset", Label = "Load")]
		[InlineButton(nameof(SaveClickHandler), ShowIf = "@!_asset", Label = "SaveAs")]
		private TextAsset _asset;


		private GroundField _field;


		[ShowInInspector, ReadOnly]
		[BoxGroup("Asset")]
		private string _path;


		[MenuItem("Scene/Level Explorer")]
		private static void OpenWindow()
		{
			var window = GetWindow<LevelExplorerWindow>();
			window.position = GUIHelper.GetEditorWindowRect().AlignCenter(800, 600);
			window.Init();
		}


		private void Init()
		{
			_field = FindObjectOfType<GroundField>();
		}


		protected override IEnumerable<object> GetTargets()
		{
			if (_field == null)
				Init();

			yield return this;
		}


		private void ClearPath()
		{
			_path = null;
			_asset = null;
		}


		private void SaveClickHandler()
		{
			// Костыль, чтобы обойти проблемы с UI Odin'а и сохранением

			SaveAsset();
			SaveAsset();
		}


		private void SaveAsset()
		{
			Debug.Log("Saving...");

			if (string.IsNullOrEmpty(_path))
			{
				_path = GetSavePathDialog();

				if (string.IsNullOrEmpty(_path))
				{
					_asset = null;

					return;
				}
			}

			_asset = WriteLevel(_path, _field.GenerateDto());

			Debug.Log("Saved!");
		}


		private void LoadAsset()
		{
			Debug.Log("Loading");

			var fileContent = File.ReadAllText(_path);
			var dto = JsonConvert.DeserializeObject<LevelMapDto>(fileContent, _Settings);

			FindObjectOfType<GroundField>().LoadFromDto(dto);

			Debug.Log("Loaded");
		}


		[CanBeNull]
		private string GetSavePathDialog()
		{
			var result = EditorUtility.SaveFilePanelInProject("Save level", "level", FileExtensions.Level, "Save file", "Assets/_src/Resources/Levels");

			if (string.IsNullOrEmpty(result))
			{
				Debug.LogWarning("No path selected");

				return null;
			}

			return result;
		}


		private static TextAsset WriteLevel(string path, LevelMapDto data)
		{
			var json = JsonConvert.SerializeObject(data, Formatting.Indented, _Settings);

			using (var file = new StreamWriter(path, false, Encoding.UTF8))
			{
				file.Write(json);
			}

			var asset = AssetDatabase.LoadMainAssetAtPath(path);
			AssetDatabase.SetLabels(asset, new[] {FileExtensions.Level});
			Debug.Log("Label");

			AssetDatabase.Refresh();

			return (TextAsset) asset;
		}


		private void OnAssetSelect(TextAsset asset)
		{
			_path = AssetDatabase.GetAssetPath(asset);
		}
	}
}
