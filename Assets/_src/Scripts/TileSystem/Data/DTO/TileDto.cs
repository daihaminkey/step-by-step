namespace StepByStep.TileSystem.DTO
{
	public class TileDto
	{
		public string Entity;
		public string Ground;


		public TileDto(string ground, string entity)
		{
			Ground = ground;
			Entity = entity;
		}
	}
}
