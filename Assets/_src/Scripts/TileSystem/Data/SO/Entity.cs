using OpenUnitySolutions.Serialization;
using Sirenix.OdinInspector;
using StepByStep.TileSystem;
using UnityEngine;


namespace StepByStep
{
	[CreateAssetMenu(fileName = "Ground", menuName = "Game/Entity")]
	public class Entity : KeyAsset
	{
		[SerializeField]
		private EntityType _entityType;


		[SerializeField, AssetsOnly, Required]
		private Sprite _sprite;


		[SerializeField, AssetsOnly, Required]
		private BaseEntity _prefab;


		public EntityType EntityType
			=> _entityType;


		public Sprite Sprite
			=> _sprite;


		public BaseEntity Prefab
			=> _prefab;
	}
}
