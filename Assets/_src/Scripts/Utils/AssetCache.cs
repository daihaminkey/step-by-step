using System;
using System.Collections.Generic;
using System.Linq;
using OpenUnitySolutions.Serialization;
using UnityEngine;


namespace StepByStep.Utils
{
	public class AssetCache
	{
		private readonly Dictionary<Type, Dictionary<string, KeyAsset>> _cache = new();


		public TAsset Get<TAsset>(string key)
			where TAsset : KeyAsset
		{
			if (string.IsNullOrEmpty(key))
				throw new Exception($"Key can't be null ({typeof(TAsset)})");

			if (!_cache.ContainsKey(typeof(TAsset)))
			{
				var all = Resources.LoadAll<TAsset>("");
				var typeByKey = all.ToDictionary(asset => asset.Key, asset => (KeyAsset) asset);
				_cache.Add(typeof(TAsset), typeByKey);
			}

			var targetDict = _cache[typeof(TAsset)];

			if (!targetDict.ContainsKey(key))
				throw new NullReferenceException($"No <{key}> asset cached for type {typeof(TAsset)}");

			return (TAsset) targetDict[key];
		}
	}
}
