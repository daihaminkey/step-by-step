using UnityEngine;


namespace StepByStep.Utils
{
	public static class ObjectExtensions
	{
		public static void ForceDestroy(this Object self)
		{
			#if UNITY_EDITOR
			if (Application.isPlaying)
				Object.Destroy(self);
			else
				Object.DestroyImmediate(self);

			#else
			Object.DestroyImmediate(self);
			#endif
		}
	}
}
