## 0.0.2

## Features
- Tiles can be generated from DTO

## Refactoring
- View and logic separated, so tests can be performed


## 0.0.1

### Features
- Grid with multi-layer tiles
- Character movement
- Usage of Input System
