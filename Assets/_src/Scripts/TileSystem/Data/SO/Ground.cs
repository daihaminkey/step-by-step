using OpenUnitySolutions.Serialization;
using Sirenix.OdinInspector;
using UnityEngine;


namespace StepByStep
{
	[CreateAssetMenu(fileName = "Ground", menuName = "Game/Ground Tile")]
	public class Ground : KeyAsset
	{
		[SerializeField, AssetsOnly, Required]
		private Sprite _sprite;


		[SerializeField]
		private bool _isBlockingMovement;


		public Sprite Sprite
			=> _sprite;


		public bool IsBlockingMovement
			=> _isBlockingMovement;
	}
}
