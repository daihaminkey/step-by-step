using System;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using StepByStep.TileSystem;
using StepByStep.TileSystem.DTO;
using StepByStep.Utils;
using UnityEngine;


namespace StepByStep
{
	public class GroundTile : MonoBehaviour, IClickable
	{
		[SerializeField, AssetsOnly, Required]
		private Sprite _overlayHighlightSprite;


		[SerializeField, ChildGameObjectsOnly, Required]
		private SpriteRenderer _image;


		[SerializeField, ChildGameObjectsOnly, Required]
		private SpriteRenderer _overlay;


		[SerializeField, AssetsOnly, CanBeNull]
		[OnValueChanged(nameof(SetData))]
		private Ground _dataSource;


		[SerializeField, SceneObjectsOnly]
		private BaseEntity _entity;


		public BaseEntity Entity
		{
			get => _entity;

			set
			{
				if (_entity == value)
					return;

				if (value != null && _entity != null)
					throw new Exception("Tile is already occupied");

				if (_entity != null && _entity is LivingEntity livingBefore)
					livingBefore.OnDeath.RemoveListener(OnContentDeathHandler);

				_entity = value;

				if (_entity != null && _entity is LivingEntity livingAfter)
					livingAfter.OnDeath.AddListener(OnContentDeathHandler);
			}
		}


		public Vector3 CenterPosition
			=> transform.position;


		public bool IsOverlayHighlighted
		{
			set
				=> _overlay.sprite = value ? _overlayHighlightSprite : null;
		}


		public bool IsBlockingMovement
		{
			get
			{
				if (_dataSource != null && _dataSource.IsBlockingMovement)
					return true;

				if (Entity != null)
					return true;

				return false;
			}
		}


		public void OnClickHandler()
			=> Debug.Log("Click");


		public void SetData(Ground source)
		{
			_image.sprite = source.Sprite;
		}


		private void OnContentDeathHandler(BaseEntity entity)
		{
			Entity = null;
		}


		public TileDto GenerateDto()
		{
			var ground = _dataSource == null
				? null
				: _dataSource.Key;

			var entity = Entity == null
				? null
				: Entity.Key;

			return new TileDto(ground, entity);
		}
	}
}
