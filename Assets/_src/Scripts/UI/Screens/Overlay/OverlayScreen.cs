using Cysharp.Threading.Tasks;
using OpenUnitySolutions.ScreenSystem;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;


namespace StepByStep.UI
{
	public class OverlayScreen : BaseScreen
	{
		[SerializeField, ChildGameObjectsOnly, Required]
		private TMP_Text _playerHealth;


		public override async UniTask OnShow()
		{
			await base.OnShow();

			_playerHealth.text = Game.Player.Health.ToString();
			Game.Player.OnHealthChange.AddListener(OnPlayerHealthChangeHandler);
		}


		public override async UniTask OnHide()
		{
			await base.OnHide();
			Game.Player.OnHealthChange.RemoveListener(OnPlayerHealthChangeHandler);
		}


		private void OnPlayerHealthChangeHandler(int healthBefore, int healthAfter)
		{
			_playerHealth.text = healthAfter.ToString();
		}
	}
}
