using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;


namespace StepByStep.CustomInput
{
	public class CustomInputProxy : MonoBehaviour
	{
		private bool _isActionsEnabled;
		private Func<UniTask> _playerAction;


		public async UniTask HandlePlayerAction()
		{
			_isActionsEnabled = true;

			await UniTask.WaitWhile(() => _playerAction == null);

			// ReSharper disable once PossibleInvalidOperationException
			await _playerAction();

			_playerAction = null;
			_isActionsEnabled = false;
		}


		public void PlayerMoveUp(InputAction.CallbackContext context)
		{
			if (context.started)
				HandlePlayerAction(Game.Player.MoveUp);
		}


		public void PlayerMoveDown(InputAction.CallbackContext context)
		{
			if (context.started)
				HandlePlayerAction(Game.Player.MoveDown);
		}


		public void PlayerMoveLeft(InputAction.CallbackContext context)
		{
			if (context.started)
				HandlePlayerAction(Game.Player.MoveLeft);
		}


		public void PlayerMoveRight(InputAction.CallbackContext context)
		{
			if (context.started)
				HandlePlayerAction(Game.Player.MoveRight);
		}


		private void HandlePlayerAction(Func<UniTask> function)
		{
			if (!_isActionsEnabled)
				return;

			_playerAction = function;
		}


		public void OnMouseClick(InputAction.CallbackContext context)
		{
			if (context.performed)
				Game.Camera.HandleMouseClick();
		}
	}
}
