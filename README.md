## Step by step

Turn-based game on a square grid. Made with Unity.


### Requirements
- Unity 2021.3.1f1

### Paid assets
This repository contains sources and .dlls of some paid assets.

You must acquire licences to those assets to work with this repository.

Here is the list of paid assets from Unity Asset Store:
- Odin Inspector and Serializer
- Astar Pathfinding Project
- Behavior Designer
- Automatic UI Anchoring
