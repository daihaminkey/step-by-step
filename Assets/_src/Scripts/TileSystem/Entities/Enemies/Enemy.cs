using Cysharp.Threading.Tasks;
using OpenUnitySolutions.UniTaskExtensions;
using StepByStep.TileSystem;
using StepByStep.Utils;
using UnityEngine;


namespace StepByStep.Enemies
{
	public class Enemy : LivingEntity
	{
		protected new void Start()
		{
			base.Start();

			Game.AiHub.RegisterEnemy(this);
		}


		public async UniTask MakeAiDecision()
		{
			Debug.Log($"{gameObject.name}: hmm...");

			var path = Game.Ground.FindPathBetween(_selfCoord, Game.Player.Coord);

			if (path.Count == 0)
				await DoRandomMove();
			else
				await MoveOnTile(path[0]);
		}


		private async UniTask DoRandomMove()
		{
			Debug.Log("Moving randomly");

			var freeCoords = Game.Ground.GetNonBlockingSideCoords(_selfCoord);

			if (freeCoords.Count == 0)
				return;

			var move = freeCoords.GetRandomElement();
			await MoveOnTile(move);
		}


		protected override async UniTask MoveOnTile(Vector2Int targetCoord)
		{
			if (Game.Player.Coord == targetCoord)
			{
				Game.Player.Health -= 1;

				await AnimateAttack(_selfCoord, targetCoord);
				
				if (Game.Player.IsDead)
				{
					await UniTaskPlus.Delay(_config.BetweenAnimationsGap);
					await base.MoveOnTile(targetCoord);
				}
			}
			else
				await base.MoveOnTile(targetCoord);
		}
	}
}
