using Cysharp.Threading.Tasks;
using StepByStep.UI;
using UnityEngine;


namespace StepByStep
{
	public class CoreLoop
	{
		public async UniTask Run()
		{
			await Game.ScreenManager.ShowScreen<OverlayScreen>();

			while (Game.Player.IsAlive)
			{
				await Game.Input.HandlePlayerAction();
				await Game.AiHub.HandleAi();

				Debug.Log("CoreLoop iterated!");
			}

			await Game.ScreenManager.ShowScreen<DeathScreen>();
		}
	}
}
