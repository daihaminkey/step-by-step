using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using StepByStep.TileSystem;
using StepByStep.TileSystem.DTO;
using StepByStep.Utils;
using UnityEngine;
using Utils.UnityArray;


namespace StepByStep
{
	public class GroundField : MonoBehaviour
	{
		[SerializeField, AssetsOnly, Required]
		[BoxGroup("Prefabs")]
		private GroundTile _tilePrefab;


		[SerializeField, OnValueChanged(nameof(Generate))]
		private Vector2Int _size = Vector2Int.one;


		[SerializeField, ChildGameObjectsOnly, Required]
		private Transform _tilesRoot;


		[SerializeField, ChildGameObjectsOnly, Required]
		private Transform _entitiesRoot;


		[SerializeField, ChildGameObjectsOnly, Required]
		private UnityArray2D<GroundTile> _tiles;


		[ShowInInspector, ReadOnly]
		private float _cellSize = 1;


		public Vector2Int Size
			=> _size;


		public GroundTile this[Vector2Int coord]
			=> this[coord.x, coord.y];


		public GroundTile this[int x, int y]
		{
			get
			{
				if (x >= _size.x || x < 0)
					throw new IndexOutOfRangeException($"X must be [0, {_size.x - 1}], {x} given");

				if (y >= _size.y || y < 0)
					throw new IndexOutOfRangeException($"Y must be [0, {_size.y - 1}], {y} given");

				return _tiles[x, y];
			}
		}


		#if UNITY_EDITOR


		private void OnDrawGizmos()
		{
			if (_tiles == null)
				return;

			Gizmos.color = Color.grey;

			var selfPosition = transform.position;
			var offset = new Vector2(selfPosition.x, selfPosition.y);

			for (var x = 0; x < _size.x + 1; x++)
			{
				var from = _cellSize * new Vector2(x, 0) + offset;
				var to = _cellSize * new Vector2(x, _size.y) + offset;

				from -= new Vector2(_cellSize, _cellSize) * 0.5f;
				to -= new Vector2(_cellSize, _cellSize) * 0.5f;

				Gizmos.DrawLine(from, to);
			}

			for (var y = 0; y < _size.y + 1; y++)
			{
				var from = _cellSize * new Vector2(0, y) + offset;
				var to = _cellSize * new Vector2(_size.x, y) + offset;

				from -= new Vector2(_cellSize, _cellSize) * 0.5f;
				to -= new Vector2(_cellSize, _cellSize) * 0.5f;

				Gizmos.DrawLine(from, to);
			}
		}


		#endif


		public void Generate(Vector2Int newSize)
		{
			DestroyEntities();
			ClearTiles();

			Debug.Log(newSize);

			_size = newSize;
			_tiles = new UnityArray2D<GroundTile>(_size.x, _size.y);

			for (var x = 0; x < _size.x; x++)
				for (var y = 0; y < _size.y; y++)
				{
					var position = new Vector3(x, y) * _cellSize + transform.position;
					_tiles[x, y] = PrefabSpawner.Instantiate(_tilePrefab, _tilesRoot, position);
				}
		}


		private void ClearTiles()
		{
			if (_tiles != null)
				for (var x = 0; x < _tiles.MaxX; x++)
					for (var y = 0; y < _tiles.MaxY; y++)
					{
						if (Application.isPlaying)
							Destroy(_tiles[x, y].gameObject);
						else
							DestroyImmediate(_tiles[x, y].gameObject);

						_tiles[x, y] = null;
					}

			foreach (Transform child in _tilesRoot)
				if (Application.isPlaying)
					Destroy(child.gameObject);
				else
					DestroyImmediate(child.gameObject);
		}


		private void ForceUpdateEntitiesPosition()
		{
			ClearEntities();

			foreach (var entity in GetComponentsInChildren<BaseEntity>())
				entity.PlaceOnInitialPosition();
		}


		private void ClearEntities()
		{
			if (_tiles == null)
				throw new NullReferenceException("Tiles field can't be null");

			for (var x = 0; x < _tiles.MaxX; x++)
				for (var y = 0; y < _tiles.MaxY; y++)
					_tiles[x, y].Entity = null;
		}


		[Button]
		private void DestroyEntities()
		{
			foreach (var entity in GetComponentsInChildren<BaseEntity>())
				entity.gameObject.ForceDestroy();
		}


		public bool IsCoordInBorders(Vector2Int targetCoord)
		{
			if (targetCoord.y >= Size.y)
				return false;

			if (targetCoord.y < 0)
				return false;

			if (targetCoord.x >= Size.x)
				return false;

			if (targetCoord.x < 0)
				return false;

			return true;
		}


		public bool IsCoordBlockingMovement(Vector2Int targetCoord)
		{
			if (!IsCoordInBorders(targetCoord))
				return true;

			return this[targetCoord].IsBlockingMovement;
		}


		public bool IsCoordNotBlockingMovement(Vector2Int targetCoord)
			=> !IsCoordBlockingMovement(targetCoord);


		public List<Vector2Int> GetNonBlockingSideCoords(Vector2Int center)
		{
			var result = GetSideCoords(center);

			return result.Where(coord => !IsCoordBlockingMovement(coord)).ToList();
		}


		public List<Vector2Int> GetSideCoords(Vector2Int center)
		{
			if (!IsCoordInBorders(center))
				throw new Exception($"Coord {center} out of borders");

			List<Vector2Int> result = new()
			{
				center + Vector2Int.up,
				center + Vector2Int.down,
				center + Vector2Int.left,
				center + Vector2Int.right,
			};

			return result.Where(IsCoordInBorders).ToList();
		}


		[Button]
		public List<Vector2Int> FindPathBetween(Vector2Int from, Vector2Int to)
		{
			if (from == to)
				return new List<Vector2Int>();

			Debug.Log($"{from} -> ${to}");
			var border = new List<Vector2Int> {from};
			var toRemove = new List<Vector2Int>();
			var toAdd = new List<Vector2Int>();

			var depthMap = new (int depth, Vector2Int from)[_size.x, _size.y];

			for (var x = 0; x < _size.x; x++)
				for (var y = 0; y < _size.y; y++)
					depthMap[x, y] = (int.MaxValue, default);

			depthMap[from.x, from.y] = (0, from);

			while (border.Count > 0)
			{
				foreach (var currentBorder in border)
				{
					var depth = depthMap[currentBorder.x, currentBorder.y].depth + 1;
					var near = GetSideCoords(currentBorder);

					foreach (var nearToBorder in near)
					{
						if (IsCoordBlockingMovement(nearToBorder) && nearToBorder != to)
							continue;

						if (depthMap[nearToBorder.x, nearToBorder.y].depth > depth)
						{
							depthMap[nearToBorder.x, nearToBorder.y] = (depth, currentBorder);
							toAdd.Add(nearToBorder);
						}
					}

					toRemove.Add(currentBorder);
				}

				foreach (var coord in toRemove)
					border.Remove(coord);

				foreach (var coord in toAdd)
					border.Add(coord);

				toRemove.Clear();
				toAdd.Clear();
			}

			if (depthMap[to.x, to.y].depth == int.MaxValue)
				return new List<Vector2Int>();

			List<Vector2Int> result = new();
			var currentCoord = to;

			while (currentCoord != from)
			{
				result.Add(currentCoord);
				currentCoord = depthMap[currentCoord.x, currentCoord.y].from;
			}

			result.Reverse();

			return result;
		}


		public (Vector3 position, Vector2Int coord) GetClosestTileCenter(Vector3 position)
		{
			int CalculateCoord(float positionAxis)
				=> (int) (positionAxis / _cellSize);

			var selfPosition = transform.position;
			var offset = new Vector3(selfPosition.x % _cellSize, selfPosition.y % _cellSize, selfPosition.z);

			var coord = new Vector2Int(CalculateCoord(position.x), CalculateCoord(position.y));
			var center = new Vector3(coord.x * _cellSize, coord.y * _cellSize, position.z) + offset;

			return (center, coord);
		}


		public LevelMapDto GenerateDto()
		{
			ForceUpdateEntitiesPosition();

			var map = new TileDto[_size.x, _size.y];

			for (var x = 0; x < _size.x; x++)
				for (var y = 0; y < _size.y; y++)
					map[x, y] = _tiles[x, y].GenerateDto();

			return new LevelMapDto(map);
		}


		public void LoadFromDto(LevelMapDto dto)
		{
			BaseEntity CreateEntity(string key, Vector2Int coord)
			{
				if (string.IsNullOrEmpty(key))
					return null;

				var source = Game.Cache.Get<Entity>(key);
				var entity = Instantiate(source.Prefab, _entitiesRoot);
				entity.SetInitialData(source, coord);

				return entity;
			}

			Generate(new Vector2Int(dto.Map.GetLength(0), dto.Map.GetLength(1)));

			for (var x = 0; x < _size.x; x++)
				for (var y = 0; y < _size.y; y++)
				{
					// TODO выставлять граунд
					var entityKey = dto.Map[x, y].Entity;
					_tiles[x, y].Entity = CreateEntity(entityKey, new Vector2Int(x, y));
				}
		}
	}
}
