using System;
using System.Collections.Generic;
using UnityEngine;


namespace Utils.UnityArray
{
	[Serializable]
	public class UnityArray<TContent>
	{
		[SerializeField]
		private List<TContent> _data;


		public readonly int Length;


		public UnityArray(int length)
		{
			Length = length;

			_data = new List<TContent>();

			for (var i = 0; i < length; i++)
				_data.Add(default);
		}


		public TContent this[int i]
		{
			get => _data[i];
			set => _data[i] = value;
		}
	}
}
