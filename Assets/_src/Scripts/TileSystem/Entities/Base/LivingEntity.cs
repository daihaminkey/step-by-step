using Cysharp.Threading.Tasks;
using DG.Tweening;
using OpenUnitySolutions.UniTaskExtensions;
using UnityEngine;
using UnityEngine.Events;


namespace StepByStep.TileSystem
{
	public class LivingEntity : BaseEntity
	{
		[SerializeField]
		private int _health = 1;


		public UnityEvent<BaseEntity> OnDeath { get; } = new();


		public UnityEvent<int, int> OnHealthChange { get; } = new();


		public int Health
		{
			get => _health;

			set
			{
				if (_health == value)
					return;

				var oldValue = _health;

				_health = value;

				if (_health < 0)
					_health = 0;

				OnHealthChange.Invoke(oldValue, _health);

				if (_health == 0)
					Die();
				else
					AnimateDamage().ForgetWithHandler();
			}
		}


		public bool IsDead
			=> Health <= 0;


		public bool IsAlive
			=> Health > 0;

		
		protected virtual async UniTask AnimateDamage()
		{
			var halfTime = _config.TakingDamageAnimationTime / 2f;

			var task = _spriteRenderer
				.DOColor(Color.red, halfTime);
			
			await task.AsyncWaitForCompletion();

			task = _spriteRenderer
				.DOColor(Color.white, halfTime);
			
			await task.AsyncWaitForCompletion();
		}


		protected virtual async UniTask AnimateAttack(Vector2Int fromCoord, Vector2Int toCoord)
		{
			var from = Game.Ground[fromCoord].CenterPosition;
			var to = Game.Ground[toCoord].CenterPosition;

			var center = (to - from) / 2f;

			var task = transform.DOPunchPosition(center, _config.MovementTime);

			await task.AsyncWaitForCompletion();
		}


		private void Die()
		{
			OnDeath.Invoke(this);
			Destroy(gameObject);
		}
	}
}
