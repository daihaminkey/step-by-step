using Sirenix.OdinInspector;
using StepByStep.TileSystem;
using UnityEngine;
using Zenject;


[CreateAssetMenu(fileName = "GameConfigInstaller", menuName = "Installers/GameConfigInstaller")]
public class GameConfigInstaller : ScriptableObjectInstaller<GameConfigInstaller>
{
	[SerializeField, AssetsOnly, Required]
	private EntityConfig _entityConfig;
	
	public override void InstallBindings()
	{
		Container.Bind<EntityConfig>().FromInstance(_entityConfig);
	}
}
