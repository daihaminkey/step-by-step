namespace StepByStep.Utils
{
	public interface IClickable
	{
		void OnClickHandler();
	}
}
