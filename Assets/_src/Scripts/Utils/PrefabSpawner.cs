using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


namespace StepByStep.Utils
{
	public static class PrefabSpawner
	{
		public static T Instantiate<T>(T prefab, Transform parent, Vector3? position = null)
			where T : MonoBehaviour
		{
			#if UNITY_EDITOR
			if (Application.isPlaying)
				return RuntimeInstantiate(prefab, parent, position);

			return EditorPrefabInstantiate(prefab, parent, position);
			#else
			return RuntimeInstantiate(prefab, parent, position);
			#endif
		}


		private static T RuntimeInstantiate<T>(T prefab, Transform parent, Vector3? position = null)
			where T : MonoBehaviour
		{
			if (position.HasValue)
				return Object.Instantiate(prefab, position.Value, Quaternion.identity, parent);

			return Object.Instantiate(prefab, parent);
		}


		#if UNITY_EDITOR


		private static T EditorPrefabInstantiate<T>(T prefab, Transform parent, Vector3? position = null)
			where T : MonoBehaviour
		{
			var result = ((GameObject) PrefabUtility
					.InstantiatePrefab(prefab.gameObject, parent))
				.GetComponent<T>();

			if (position.HasValue)
				result.transform.position = position.Value;

			return result;
		}


		#endif
	}
}
