using StepByStep.Utils;
using UnityEngine;
using UnityEngine.InputSystem;


namespace StepByStep
{
	public class CameraController : MonoBehaviour
	{
		private Camera _selfCamera;


		private void Awake()
		{
			_selfCamera = GetComponent<Camera>();
		}


		public void HandleMouseClick()
		{
			Vector2 mousePos = _selfCamera.ScreenToWorldPoint(Mouse.current.position.ReadValue());
			var hit = Physics2D.Raycast(mousePos, Vector2.zero);

			if (!hit)

				// _mapEditor.UnselectTile();
				return;

			var clickable = hit.transform.GetComponent<IClickable>();

			if (clickable == null)

				//_mapEditor.UnselectTile();
				return;

			clickable.OnClickHandler();
		}
	}
}
