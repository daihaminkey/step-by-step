using Sirenix.OdinInspector;
using UnityEngine;


namespace StepByStep.TileSystem
{
	[CreateAssetMenu(fileName = "entity_config", menuName = "Game/Config/Entity")]
	public class EntityConfig : ScriptableObject
	{
		[SerializeField]
		[Range(0.05f, 1f)]
		private float _movementTime;


		[SerializeField]
		[PropertyRange(0, nameof(_movementTime))]
		private float _takingDamageAnimationTime;


		[SerializeField]
		[PropertyRange(0, nameof(_movementTime))]
		private float _betweenAnimationsGap;
		
		
		public float MovementTime
			=> _movementTime;


		public float TakingDamageAnimationTime
			=> _takingDamageAnimationTime;


		public float BetweenAnimationsGap
			=> _betweenAnimationsGap;
	}
}
