using System;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using OpenUnitySolutions.UniTaskExtensions;
using Sirenix.OdinInspector;
using StepByStep.Utils;
using UnityEngine;
using Zenject;


namespace StepByStep.TileSystem
{
	[ExecuteAlways]
	public class BaseEntity : MonoBehaviour
	{
		[Inject]
		[SerializeField, AssetsOnly]
		[BoxGroup(InspectorGroups.Injected)]
		protected EntityConfig _config;

		
		[SerializeField, AssetsOnly, Required]
		private Entity _source;


		[SerializeField]
		private Vector2Int _initialCoord;


		[SerializeField, ChildGameObjectsOnly, Required]
		protected SpriteRenderer _spriteRenderer;


		[SerializeField]
		private bool _isPushable;


		[ShowInInspector, ReadOnly]
		protected Vector2Int _selfCoord;


		public EntityType EntityType
			=> _source.EntityType;


		public string Key
			=> _source.Key;


		public Vector2Int Coord
			=> _selfCoord;


		protected void Start()
		{
			_spriteRenderer.sprite = _source.Sprite;
			PlaceOnInitialPosition();
		}


		protected void Update()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				UpdateInitialPosition();
			#endif
		}


		public void SetInitialData(Entity source, Vector2Int coord)
		{
			_source = source;
			_initialCoord = coord;

			_spriteRenderer.sprite = _source.Sprite;
		}


		public void PlaceOnInitialPosition()
		{
			if (Game.Ground.IsCoordBlockingMovement(_initialCoord) && Game.Ground[_initialCoord].Entity != this)
				throw new Exception($"Can't place entity {gameObject.name} on start position {_initialCoord}: {Game.Ground[_initialCoord].Entity} is on the way");

			MoveOnTile(_initialCoord).ForgetWithHandler();
		}


		protected virtual async UniTask MoveOnTile(Vector2Int targetCoord)
		{
			Game.Ground[_selfCoord].Entity = null;

			_selfCoord = targetCoord;
			Game.Ground[_selfCoord].Entity = this;
			
			var targetPosition = Game.Ground[_selfCoord].CenterPosition;
			await AnimateMovement(targetPosition);
		}


		protected virtual async UniTask AnimateMovement(Vector3 targetPosition)
		{
			var time = _config.MovementTime;
			transform.DOMove(targetPosition, time);
			await UniTaskPlus.Delay(time);
		}


		[Button]
		public bool PushFrom(Vector2Int fromCoord, EntityType fromEntityType)
		{
			if (!_isPushable)
				return false;

			var offset = _selfCoord - fromCoord;

			if (Math.Abs(offset.x + offset.y) != 1)
				throw new Exception($"Can't push {_selfCoord} from {fromCoord}");

			var targetCoord = _selfCoord + offset;

			if (Game.Ground.IsCoordNotBlockingMovement(targetCoord))
			{
				MoveOnTile(targetCoord).ForgetWithHandler();

				return true;
			}

			return false;
		}


		#if UNITY_EDITOR


		private void UpdateInitialPosition()
		{
			var newData = Game.Ground.GetClosestTileCenter(transform.position);
			_initialCoord = newData.coord;
			transform.position = newData.position;
		}


		#endif
	}
}
