using Cysharp.Threading.Tasks;
using OpenUnitySolutions.UniTaskExtensions;
using StepByStep.TileSystem;
using UnityEngine;


namespace StepByStep.Player
{
	public class PlayerCharacter : LivingEntity
	{
		public async UniTask MoveUp()
			=> await TryMoveToCoordByDelta(0, +1);


		public async UniTask MoveDown()
			=> await TryMoveToCoordByDelta(0, -1);


		public async UniTask MoveLeft()
			=> await TryMoveToCoordByDelta(-1, 0);


		public async UniTask MoveRight()
			=> await TryMoveToCoordByDelta(+1, 0);


		private async UniTask TryMoveToCoordByDelta(int x, int y)
		{
			var targetCoord = _selfCoord + new Vector2Int(x, y);
			await TryMoveToCoord(targetCoord);
		}


		private async UniTask TryMoveToCoord(Vector2Int targetCoord)
		{
			if (!Game.Ground.IsCoordInBorders(targetCoord))
				return;

			if (Game.Ground.IsCoordNotBlockingMovement(targetCoord))
				await MoveOnTile(targetCoord);
			else if (Game.Ground[targetCoord].Entity != null)
			{
				var tileContent = Game.Ground[targetCoord].Entity;

				if (tileContent is LivingEntity living)
				{
					living.Health -= 1;
					
					await AnimateAttack(_selfCoord, targetCoord);

					if (living.IsDead)
					{
						await UniTaskPlus.Delay(_config.BetweenAnimationsGap);
						await MoveOnTile(targetCoord);
					}
				}
				else
				{
					var isPushed = tileContent.PushFrom(_selfCoord, EntityType.Player);

					if (isPushed)
						await MoveOnTile(targetCoord);
				}
			}
		}
	}
}
