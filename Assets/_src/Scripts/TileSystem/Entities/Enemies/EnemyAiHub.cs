using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using StepByStep.TileSystem;
using UnityEngine;


namespace StepByStep.Enemies
{
	public class EnemyAiHub : MonoBehaviour
	{
		[ShowInInspector, ReadOnly]
		private HashSet<Enemy> _enemies = new();


		public void RegisterEnemy(Enemy enemy)
		{
			_enemies.Add(enemy);
			enemy.OnDeath.AddListener(OnEnemyDeath);
		}


		public async UniTask HandleAi()
		{
			foreach (var enemy in _enemies)
				await enemy.MakeAiDecision();
		}


		private void OnEnemyDeath(BaseEntity entity)
		{
			var enemy = (Enemy) entity;

			_enemies.Remove(enemy);
		}
	}
}
