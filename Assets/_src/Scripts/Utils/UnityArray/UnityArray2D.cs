using System;
using System.Collections.Generic;
using UnityEngine;


namespace Utils.UnityArray
{
	[Serializable]
	public class UnityArray2D<TContent>
	{
		[SerializeField]
		private List<UnityArray<TContent>> _data;


		public readonly int MaxX;

		public readonly int MaxY;


		public UnityArray2D(int maxX, int maxY)
		{
			MaxX = maxX;
			MaxY = maxY;

			_data = new List<UnityArray<TContent>>();

			for (var x = 0; x < maxX; x++)
				_data.Add(new UnityArray<TContent>(maxY));
		}


		public TContent this[int x, int y]
		{
			get => _data[x][y];
			set => _data[x][y] = value;
		}
	}
}
