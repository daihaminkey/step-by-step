using System;
using OpenUnitySolutions.ScreenSystem;
using OpenUnitySolutions.UniTaskExtensions;
using Sirenix.OdinInspector;
using StepByStep.CustomInput;
using StepByStep.Enemies;
using StepByStep.Player;
using StepByStep.Utils;
using UnityEngine;


namespace StepByStep
{
	[ExecuteAlways]
	public class Game : MonoBehaviour
	{
		private static Game _instance;


		[SerializeField, SceneObjectsOnly, Required]
		private CameraController _camera;


		[SerializeField, SceneObjectsOnly, Required]
		private GroundField _ground;


		[SerializeField, SceneObjectsOnly, Required]
		private EnemyAiHub _aiHub;


		[SerializeField, SceneObjectsOnly, Required]
		private CustomInputProxy _input;


		[SerializeField, SceneObjectsOnly, Required]
		private ScreenManager _screenManager;


		private readonly Lazy<AssetCache> _cache = new(() => new AssetCache());

		private CoreLoop _coreLoop;


		[SceneObjectsOnly, Required]
		private readonly Lazy<PlayerCharacter> _player = new(FindObjectOfType<PlayerCharacter>);


		private static Game Instance
		{
			get
			{
				#if UNITY_EDITOR
				if (_instance == null && !Application.isPlaying)
					_instance = FindObjectOfType<Game>();
				#endif
				return _instance;
			}
		}


		public static PlayerCharacter Player
			=> Instance._player.Value;


		public static CameraController Camera
			=> Instance._camera;


		public static GroundField Ground
			=> Instance._ground;


		public static EnemyAiHub AiHub
			=> Instance._aiHub;


		public static CustomInputProxy Input
			=> Instance._input;


		public static ScreenManager ScreenManager
			=> Instance._screenManager;


		public static AssetCache Cache
			=> Instance._cache.Value;


		private void Awake()
		{
			_instance = this;
			_coreLoop = new CoreLoop();
		}


		private void Start()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			_coreLoop.Run().ForgetWithHandler();
		}
	}
}
