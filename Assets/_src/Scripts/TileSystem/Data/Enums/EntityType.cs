namespace StepByStep.TileSystem
{
	public enum EntityType
	{
		None,
		Player,
		Enemy,
		Obstacle,
	}
}
