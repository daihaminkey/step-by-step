using System;
using System.Collections.Generic;
using System.Linq;


namespace StepByStep.Utils
{
	public static class EnumerableExtensions
	{
		public static T GetRandomElement<T>(this IEnumerable<T> list)
		{
			var enumerable = list as T[] ?? list.ToArray();

			if (!enumerable.Any())
				throw new IndexOutOfRangeException("Empty collection");

			var hashCode = Math.Abs(Guid.NewGuid().GetHashCode());

			return enumerable.ElementAt(hashCode % enumerable.Count());
		}
	}
}
